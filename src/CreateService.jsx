import { Button, Container, Input } from "@nextui-org/react";
import { useRef, useState } from "react";
import axios from "axios";



export let servicesData;
function CreateService(props) {

	const [city, setCity] = useState("")
	const count = useRef(0)


	const handleSubmit = async event => {
		event.preventDefault()
		try {
			await axios.post("http://127.0.0.1:8000/api/services", {
				city: city
			}, {
				headers: {
					'Content-Type': 'application/json'
				}
			})

			count.current = count.current + 1
			props.onServicesDataTrigger(count.current)
			
		} catch (err) {
			console.log(err)
		}
	}


	return (
		<Container css={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", width: "50vw", border: "1px solid grey", padding: "40px", borderRadius: "10px" }}>
			{/* <Button onPress={handleButtonClick}>click</Button> */}
			<form onSubmit={handleSubmit} style={{ display: "flex", flexDirection: "column", gap: "2.5rem", width: "70%" }} action="" method="post">
				<Input
					clearable
					underlined
					labelPlaceholder="Ville"
					value={city}

					onChange={(e) => setCity(e.target.value)}
					color="primary" />

			

				<Button
					type="submit"
				>
					Creer Service
				</Button>
			</form>

		</Container>

	);
}
export default CreateService;