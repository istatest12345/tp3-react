import { Spacer, Table, Button, Container } from "@nextui-org/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";


function AfficherService() {
	const params = useParams()
	const [serviceData, setServiceData] = useState({})

	useEffect(() => {
		const getService = async () => {
			let response = await axios.get(`http://127.0.0.1:8000/api/services/${params.id}`)
			let service = await response.data
			setServiceData(service)
		}
		getService()
	}, [])

	return (
		<Container css={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", width: "fit-content", border: "1px solid grey", padding: "40px", borderRadius: "10px" }}>
			<Table
				shadow={false}
				aria-label="Example table with dynamic content & infinity pagination"
				css={{ width: "60vw" }}
				color="secondary"
			>
				<Table.Header>
					<Table.Column key={0}>Code Service</Table.Column>
					<Table.Column key={1}>Ville</Table.Column>
					<Table.Column key={2}>Charge</Table.Column>
				</Table.Header>
				<Table.Body>
					<Table.Row key={serviceData.id}>
						<Table.Cell>{serviceData.id}</Table.Cell>
						<Table.Cell>{serviceData.city}</Table.Cell>
						<Table.Cell>{serviceData.employees?.reduce((total, curr) => total + curr.salaire, 0)}</Table.Cell>
					</Table.Row>
				</Table.Body>
			</Table>
			<Spacer />
			<h3>Liste des employees</h3>
			{/* <Spacer /> */}
			<Table
				// bordered
				shadow={true}
				color="secondary"
				aria-label="Example pagination  table"
				css={{
					height: "auto",
					width: "60vw",
				}}
				selectionMode="single"
			>
				<Table.Header>
					<Table.Column>ID</Table.Column>
					<Table.Column>Nom</Table.Column>
					<Table.Column>Salaire</Table.Column>
					<Table.Column>Action</Table.Column>
				</Table.Header>
				<Table.Body>
					{serviceData.employees?.map((employee, index) => {
						return (
							<Table.Row key={index}>
								<Table.Cell>{employee.id}</Table.Cell>
								<Table.Cell>{employee.nom}</Table.Cell>
								<Table.Cell>{employee.salaire}</Table.Cell>
								<Table.Cell>
									<Button className='inline' color="warning" auto>Edit</Button>
									<Button className='inline' color="error" auto>Delete</Button>
								</Table.Cell>
							</Table.Row>
						);
					})}

				</Table.Body>
				<Table.Pagination
					shadow
					noMargin
					align="center"
					rowsPerPage={5}
				/>
			</Table>
		</Container>
	);
}

export default AfficherService;