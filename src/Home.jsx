import { Button, Input, Modal, Spacer, Table, Text } from '@nextui-org/react';
import axios from 'axios';
import { useRef, useState } from 'react';
import { Link } from 'react-router-dom';


function Home(props) {
	const count = useRef(0)
	const [visible, setVisible] = useState(false);
	const [city, setCity] = useState("");
	const [ id, setId ] = useState(null)
	const handler = (id) => {
		setVisible(true)
		setId(id)
	}
	const closeHandler = () => {
		setVisible(false);
		console.log("closed");
	};

	const handleDelete = async id => {
		await axios.delete(`http://127.0.0.1:8000/api/services/${id}`)
		count.current += 1
		props.onServicesDataTrigger(count.current)
	}
	
	const handleUpdate = async id => {
		await axios.put(`http://127.0.0.1:8000/api/services/${id}`, {city: city})
		count.current += 1
		props.onServicesDataTrigger(count.current)
		setVisible(false);
	}

	return (
		<>
			<Link to="add">Add</Link>

			<Spacer />
			<Table
				bordered
				shadow={false}
				color="secondary"
				aria-label="Example pagination  table"
				css={{
					height: "auto",
					minWidth: "80%",
				}}
				selectionMode="single"
			>
				<Table.Header>
					<Table.Column>Code</Table.Column>
					<Table.Column>Nbre Employes</Table.Column>
					<Table.Column>Ville</Table.Column>
					<Table.Column>Total Salaire</Table.Column>
					<Table.Column>Action</Table.Column>
				</Table.Header>
				<Table.Body>
					{props.services.map((service, index) => {
						return (
							<Table.Row key={index}>
								<Table.Cell>{service.id}</Table.Cell>
								<Table.Cell>{service.employees.length}</Table.Cell>
								<Table.Cell>{service.city}</Table.Cell>
								<Table.Cell>{service.employees.reduce((total, current) => {
									return (
										total + current.salaire
									);
								}, 0)}</Table.Cell>
								<Table.Cell>
									<Button className='inline' color="warning" onPress={() => handler(service.id)} auto>Edit</Button>
									<Button className='inline' color="error" onPress={() => handleDelete(service.id)} auto>Delete</Button>
								</Table.Cell>
							</Table.Row>
						);
					})}

				</Table.Body>
				<Table.Pagination
					shadow
					noMargin
					align="center"
					rowsPerPage={5}

				/>
			</Table>
			<Modal
				closeButton
				blur
				aria-labelledby="modal-title"
				open={visible}
				onClose={closeHandler}
			>
				<Modal.Header>
					<Text id="modal-title" size={18}>
						Welcome to
						<Text b size={18}>
							NextUI
						</Text>
					</Text>
				</Modal.Header>
				<Modal.Body>
					<Input
						clearable
						bordered
						fullWidth
						color="primary"
						size="lg"
						value={city}
						onChange={(e) => setCity(e.target.value)}
						placeholder="Ville"
					/>
				</Modal.Body>
				<Modal.Footer>
					<Button auto flat color="warning" onPress={() => handleUpdate(id)}>
						Update
					</Button>
					<Button auto flat color="error" onPress={closeHandler}>
						Close
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
}

export default Home;