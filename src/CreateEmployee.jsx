import { Button, Container, Dropdown, Input, Modal, Spacer, Table } from "@nextui-org/react";
import axios from "axios";
import { useState, useRef } from "react";

function CreateEmployee(props) {

	const [nom, setNom] = useState("")
	const [salaire, setSalaire] = useState("")
	const [selected, setSelected] = useState(0)
	const count = useRef(0)
	



	const handleSubmit = async event => {
		event.preventDefault()
		try {
			await axios.post("http://127.0.0.1:8000/api/employees", {
				nom: nom,
				service_id: selected,
				salaire: Number(salaire)
			}, {
				headers: {
					'Content-Type': 'application/json'
				}
			})

			count.current = count.current + 1
			props.onEmployeesDataTrigger(count.current)

		} catch (err) {
			console.log(err)
		}
	}

	const getSelectedService = e => {
		let arr = Array.from(e)
		let num = Number(arr)
		setSelected(num)
	}

	return (
		<>
			<Container css={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", width: "50vw", border: "1px solid grey", padding: "40px", borderRadius: "10px" }}>
				<form onSubmit={handleSubmit} style={{ display: "flex", flexDirection: "column", gap: "2.5rem", width: "70%" }} action="" method="post">
					<Input
						clearable
						underlined
						labelPlaceholder="Nom"
						value={nom}
						name="nom"
						onChange={(e) => setNom(e.target.value)}
						color="primary" />
					<Input
						clearable
						underlined
						labelPlaceholder="Salaire"
						name="salaire"
						value={salaire}
						onChange={(e) => setSalaire(e.target.value)}
						color="primary" />

					<Dropdown>
						<Dropdown.Button flat>Select employee</Dropdown.Button>
						<Dropdown.Menu
							aria-label="Multiple selection actions"
							color="primary"
							disallowEmptySelection
							selectionMode="single"
							onSelectionChange={getSelectedService}
						>
							{props.services.map(service => {
								return (
									<Dropdown.Item key={service.id}>{service.city}</Dropdown.Item>
								);
							})}

						</Dropdown.Menu>
					</Dropdown>

					<Button
						type="submit"
					>
						Creer Employee
					</Button>
				</form>
			</Container>
			<Spacer />
			<Container css={{ display: "flex", justifyContent: "center", alignItems: "center", width: "fit-content", border: "1px solid grey", borderRadius: "10px" }}>
				<Table
					// bordered
					shadow={true}
					color="secondary"
					aria-label="Example pagination  table"
					css={{
						height: "auto",
						width: "60vw",
					}}
					selectionMode="single"
				>
					<Table.Header>
						<Table.Column>Id</Table.Column>
						<Table.Column>Nom</Table.Column>
						<Table.Column>Salaire</Table.Column>
						<Table.Column>Action</Table.Column>
					</Table.Header>
					<Table.Body>
						{props.employees.map((employee, index) => {
							return (
								<Table.Row key={index}>
									<Table.Cell>{employee.id}</Table.Cell>
									<Table.Cell>{employee.nom}</Table.Cell>
									<Table.Cell>{employee.salaire}</Table.Cell>
									<Table.Cell>
										<Button className='inline' color="warning" auto>Edit</Button>
										<Button className='inline' color="error" auto>Delete</Button>
									</Table.Cell>
								</Table.Row>
							);
						})}

					</Table.Body>
					<Table.Pagination
						shadow
						noMargin
						align="center"
						rowsPerPage={5}
						onPageChange={(page) => console.log({ page })}
					/>
				</Table>
			</Container>
			
		</>

	);
}

export default CreateEmployee;