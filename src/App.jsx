import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import SharedLayout from './components/SharedLayout'
import Home from './Home'
import { NextUIProvider, createTheme } from '@nextui-org/react'
import CreateService from './CreateService'
import CreateEmployee from './CreateEmployee'
import AfficherService from './AfficherService'
import { useEffect, useState } from 'react'
import axios from 'axios'

const myDarkTheme = createTheme({
	type: "dark",
	theme: {}
})

function App() {

	const [ serviceData, setServiceData ] = useState([])
	const [ employeeData, setEmployeeData ] = useState([])
	const [ serviceTrigger, setServiceTrigger ] = useState(0)
	const [ employeeTrigger, setEmployeeTrigger ] = useState(0)
	
	const handleServicesDataTrigger = (trigger) => {
		setServiceTrigger(trigger)
	}
	const handleEmployeesDataTrigger = (trigger) => {
		setEmployeeTrigger(trigger)
	}
	
	useEffect(() => {
		const getServices = async () => {
			try {
				let servicesResponse = await axios.get("http://127.0.0.1:8000/api/services")
				let data = await servicesResponse.data
				setServiceData(data)
			} catch (err) {
				console.log(err)
			}
		}
		getServices()
	}, [serviceTrigger])

	useEffect(() => {
		const getEmployees = async () => {
			try {
				let employeesResponse = await axios.get("http://127.0.0.1:8000/api/employees")
				let data = await employeesResponse.data
				setEmployeeData(data)
			} catch (err) {
				console.log(err)
			}
		}
		getEmployees()
	}, [employeeTrigger])

	return (
		<NextUIProvider theme={myDarkTheme}>

			<BrowserRouter>
				<Routes>
					<Route path='/' element={<SharedLayout />}>
						<Route index element={<Home services={serviceData} onServicesDataTrigger={handleServicesDataTrigger} />} />
						<Route path='add' element={<CreateService services={serviceData} onServicesDataTrigger={handleServicesDataTrigger} />} />
						<Route path='add/employee' element={<CreateEmployee services={serviceData}  employees={employeeData} onEmployeesDataTrigger={handleEmployeesDataTrigger} />} />
						<Route path='contact' element={<div>contact</div>} />
						<Route path='about' element={<div>about</div>} />
						<Route path='services/:id' element={<AfficherService />} />
						<Route path='services/:id/edit' element={<div>editing a serivice</div>} />
						<Route path='services/:id/delete' element={<div>deliting a serivice</div>} />
						<Route path=':id/update' element={<div>updating a serivice</div>} />
					</Route>
					<Route path='*' element={<div>404: page not found</div>} />
				</Routes>
			</BrowserRouter>

		</NextUIProvider>
	)
}

export default App
