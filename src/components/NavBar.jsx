import React from "react";
import { NavLink } from "react-router-dom";
import { Navbar, Button, Link, Text } from "@nextui-org/react";


export default function NavBar() {

	return (

		<Navbar id="navigationBar" variant="sticky">
			<Navbar.Brand>
				<Text b color="inherit" hideIn="xs">
					Services
				</Text>
			</Navbar.Brand>
			<Navbar.Content hideIn="xs">
				<NavLink to="/" className=""><Text css={{ "&:hover": { color: "$primary" } }}>Home</Text></NavLink>
				<NavLink to="add"><Text css={{ "&:hover": { color: "$primary" } }}>Add</Text></NavLink>
				<NavLink to="contact"><Text css={{ "&:hover": { color: "$primary" } }}>Contact</Text></NavLink>
				<NavLink to="about"><Text css={{ "&:hover": { color: "$primary" } }}>About</Text></NavLink>
			</Navbar.Content>
			<Navbar.Content>
				
			</Navbar.Content>
		</Navbar>


	)
}