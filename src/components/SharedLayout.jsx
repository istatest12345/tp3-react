import { Spacer } from "@nextui-org/react";
import { Outlet } from "react-router-dom";
import Footer from "./Footer";
import NavBar from "./NavBar";

function SharedLayout() {
	return (
		<>
			<NavBar />
			<Spacer />
			<Outlet />
			<Spacer/>
			<Footer />
		</>
	);
}

export default SharedLayout;